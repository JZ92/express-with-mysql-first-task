/* 
  if there is an error thrown in the DB, asyncMiddleware
  will pass it to next() and express will handle the error */
import raw from "../../middleware/route.async.wrapper.mjs";
// import user_model from "./user.model.mjs";
import express from "express";
import log from "@ajar/marker";
import Joi from "joi";
import connection from "../../db/mysql.connection.mjs";

const router = express.Router();

const TABLE = "MOCK_DATA";

// parse json req.body on post routes
router.use(express.json());

// CREATE NEW USER
router.post(
  "/",
  raw(async (req, res) => {
    log.obj(req.body, "create a user, req.body:");

    const schema = Joi.object({
      first_name: Joi.string().min(3).max(30).required(),
      last_name: Joi.string().min(3).max(30).required(),
      email: Joi.string().email().required(),
    });

    const { error, value } = schema.validate(req.body);
    if (error) {
      throw new Error(
        `Validation error: ${error.details.map((x) => x.message).join(", ")}`
      );
    } else {
      req.body = value;
    }

    const queryResponse = await connection.query(
      `INSERT INTO ${TABLE} (first_name, last_name, email) VALUES("${req.body.first_name}", "${req.body.last_name}", "${req.body.email}");`
    );
    const getResponse = await connection.query(
      `SELECT * FROM ${TABLE} ORDER BY ID DESC LIMIT 1`
    );
    if (queryResponse[0].affectedRows > 0) res.status(200).json(getResponse[0]);
  })
);

// GET ALL USERS
router.get(
  "/",
  raw(async (req, res) => {
    const getResponse = await connection.query(`SELECT * FROM ${TABLE};`);
    res.status(200).json(getResponse[0]);
  })
);

// GET PAGINATION
router.get(
  "/paginate/:page?/:items?",
  raw(async (req, res) => {
    log.obj(req.params, "get all users, req.params:");
    let { page = 0, items = 10 } = req.params;
    const getResponse = await connection.query(
      `SELECT * FROM ${TABLE} LIMIT ${items} OFFSET ${page};`
    );

    res.status(200).json(getResponse[0]);
  })
);

// GETS A SINGLE USER
router.get(
  "/:id",
  raw(async (req, res) => {
    const getResponse = await connection.query(
      `SELECT * FROM ${TABLE} WHERE id=${req.params.id};`
    );
    res.status(200).json(getResponse[0][0]);
  })
);

// UPDATES A WHOLE USER
router.put(
  "/:id",
  raw(async (req, res) => {
    const schema = Joi.object({
      first_name: Joi.string().min(3).max(30).required(),
      last_name: Joi.string().min(3).max(30).required(),
      email: Joi.string().email().required(),
    });

    const { error, value } = schema.validate(req.body);
    if (error) {
      throw new Error(
        `Validation error: ${error.details.map((x) => x.message).join(", ")}`
      );
    } else {
      req.body = value;
    }
    const getResponse = await connection.query(
      `SELECT * FROM ${TABLE} WHERE id=${req.params.id};`
    );
    const user = getResponse[0][0];
    const queryResponse = await connection.query(`UPDATE ${TABLE}
    SET first_name = "${req.body.first_name}", last_name = "${req.body.last_name}", email = "${req.body.email}"
    WHERE id = ${req.params.id};`);
    if (queryResponse[0].affectedRows > 0) res.status(200).json(user);
  })
);

// UPDATE SOME PROPERTIES IN USER
router.patch(
  "/:id",
  raw(async (req, res) => {
    const schema = Joi.object({
      first_name: Joi.string().min(3).max(30),
      last_name: Joi.string().min(3).max(30),
      email: Joi.string().email(),
    });

    const { error, value } = schema.validate(req.body);
    if (error) {
      throw new Error(
        `Validation error: ${error.details.map((x) => x.message).join(", ")}`
      );
    } else {
      req.body = value;
    }

    const getResponse = await connection.query(
      `SELECT * FROM ${TABLE} WHERE id=${req.params.id};`
    );
    const user = getResponse[0][0];

    let patchQuery = `UPDATE ${TABLE} SET `;
    Object.keys(req.body).map(
      (property) => (patchQuery += `${property} = "${req.body[property]}", `)
    );
    patchQuery = patchQuery.slice(0, patchQuery.length - 2);
    patchQuery += ` WHERE id = ${req.params.id};`;

    const queryResponse = await connection.query(patchQuery);

    if (queryResponse[0].affectedRows > 0) res.status(200).json(user);
  })
);

// DELETES A USER
router.delete(
  "/:id",
  raw(async (req, res) => {
    const getResponse = await connection.query(
      `SELECT * FROM ${TABLE} WHERE id=${req.params.id};`
    );
    const user = getResponse[0][0];
    const queryResponse = await connection.query(`DELETE FROM ${TABLE}
    WHERE id = ${req.params.id};`);
    if (queryResponse[0].affectedRows > 0) res.status(200).json(user);
  })
);

export default router;
